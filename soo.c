#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <time.h>

#define MAX_PATH_SIZE 1024
#define SNAPSHOT_DELIMITER "|"
#define MAX_FILES 1000 // Maximum number of files in the snapshot

// Structure to hold file metadata
struct FileMetadata
{
    char name[MAX_PATH_SIZE];
    off_t size;
    mode_t type;
    time_t last_modified;
    int verify_deleted;
};

// Function to get file metadata
void getFileMetadata(const char *path, struct FileMetadata *metadata)
{
    struct stat fileStat;
    if (stat(path, &fileStat) < 0)
    {
        perror("Error");
        exit(EXIT_FAILURE);
    }

    strncpy(metadata->name, path, sizeof(metadata->name));
    metadata->size = fileStat.st_size;
    metadata->type = fileStat.st_mode;
    metadata->last_modified = fileStat.st_mtime;
    metadata->verify_deleted = 0;
}

// Function to save file metadata snapshot to a file
void saveSnapshot(const char *snapshotFile, struct FileMetadata *metadata, int count)
{
    FILE *file = fopen(snapshotFile, "w");
    if (file == NULL)
    {
        perror("Error");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < count; i++)
    {
        fprintf(file, "%s%s%ld%s%d%s%I64d%s%d\n", metadata[i].name, SNAPSHOT_DELIMITER,
                metadata[i].size, SNAPSHOT_DELIMITER, metadata[i].type, SNAPSHOT_DELIMITER,
                metadata[i].last_modified, SNAPSHOT_DELIMITER, metadata[i].verify_deleted);
    }

    fclose(file);
}

// Function to load file metadata snapshot from a file
int loadSnapshot(const char *snapshotFile, struct FileMetadata *metadata)
{
    FILE *file = fopen(snapshotFile, "r");
    if (file == NULL)
    {
        return 0; // No snapshot found
    }

    int count = 0;
    char line[MAX_PATH_SIZE * 3]; // Assumed maximum length of a line

    while (fgets(line, sizeof(line), file) != NULL)
    {
        char *token;
        token = strtok(line, SNAPSHOT_DELIMITER);
        strncpy(metadata[count].name, token, sizeof(metadata[count].name));

        token = strtok(NULL, SNAPSHOT_DELIMITER);
        metadata[count].size = atoi(token);

        token = strtok(NULL, SNAPSHOT_DELIMITER);
        metadata[count].type = atoi(token);

        token = strtok(NULL, SNAPSHOT_DELIMITER);
        metadata[count].last_modified = atoi(token);

        count++;
    }

    fclose(file);
    return count;
}

// Function to recursively traverse directories
void traverseDirectory(const char *path, struct FileMetadata *snapshot, int *snapshotCount)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(path);
    if (dir == NULL)
    {
        perror("Error");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
        {
            char fullPath[MAX_PATH_SIZE];
            sprintf(fullPath, "%s/%s", path, entry->d_name);

            struct FileMetadata metadata;
            getFileMetadata(fullPath, &metadata);

            if (S_ISREG(metadata.type))
            {
                int foundInSnapshot = 0;
                for (int i = 0; i < *snapshotCount; i++)
                {
                    if (strcmp(metadata.name, snapshot[i].name) == 0)
                    {
                        foundInSnapshot = 1;
                        snapshot[i].verify_deleted = 1;
                        if (metadata.last_modified != snapshot[i].last_modified)
                        {
                            printf("File '%s' modified.\n", metadata.name);
                            // Update metadata in the snapshot
                            snapshot[i] = metadata;
                            snapshot[i].verify_deleted = 1;
                        }
                        break;
                    }
                }
                if (!foundInSnapshot)
                {
                    printf("File '%s' created.\n", metadata.name);
                    metadata.verify_deleted = 1;
                    // Add new file to the snapshot
                    if (*snapshotCount < MAX_FILES)
                    {
                        snapshot[*snapshotCount] = metadata;
                        (*snapshotCount)++;
                    }
                    else
                    {
                        fprintf(stderr, "Maximum number of files reached in the snapshot. Ignoring additional files.\n");
                    }
                }
            }
            if (S_ISDIR(metadata.type))
            {
                traverseDirectory(fullPath, snapshot, snapshotCount);
            }
        }
    }

    closedir(dir);
}
void checkDeleted(struct FileMetadata *snapshot, int *snapshotCount)
{
    for (int i = 0; i < (*snapshotCount); i++)
    {
        if (snapshot[i].verify_deleted == 0)
        {
            printf("File '%s' deleted.\n", snapshot[i].name);
            (*snapshotCount)--;
        }
    }
}
int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Usage: %s <directory1> [<directory2> ...]\n", argv[0]);
        return EXIT_FAILURE;
    }


    for (int i = 1; i < argc; i++)
    {

        char snapshotFile[250];
        strcpy(snapshotFile ,argv[i]);
        strcat(snapshotFile,".snapshot.txt");
        struct FileMetadata snapshot[MAX_FILES];
        int snapshotCount = loadSnapshot(snapshotFile, snapshot);

        printf("Snapshot for directory: %s\n", argv[i]);
        traverseDirectory(argv[i], snapshot, &snapshotCount);

        checkDeleted(snapshot, &snapshotCount);
        // Save the updated snapshot
        saveSnapshot(snapshotFile, snapshot, snapshotCount);
    }


    return EXIT_SUCCESS;
}
